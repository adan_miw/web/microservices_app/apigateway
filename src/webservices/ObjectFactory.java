
package webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DatabaseErrorException_QNAME = new QName("http://webservices/", "DatabaseErrorException");
    private final static QName _DeleteTask_QNAME = new QName("http://webservices/", "deleteTask");
    private final static QName _DeleteTaskResponse_QNAME = new QName("http://webservices/", "deleteTaskResponse");
    private final static QName _GetTaskByState_QNAME = new QName("http://webservices/", "getTaskByState");
    private final static QName _GetTaskByStateResponse_QNAME = new QName("http://webservices/", "getTaskByStateResponse");
    private final static QName _SaveTask_QNAME = new QName("http://webservices/", "saveTask");
    private final static QName _SaveTaskResponse_QNAME = new QName("http://webservices/", "saveTaskResponse");
    private final static QName _UpdateTask_QNAME = new QName("http://webservices/", "updateTask");
    private final static QName _UpdateTaskResponse_QNAME = new QName("http://webservices/", "updateTaskResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DatabaseErrorException }
     * 
     */
    public DatabaseErrorException createDatabaseErrorException() {
        return new DatabaseErrorException();
    }

    /**
     * Create an instance of {@link DeleteTask }
     * 
     */
    public DeleteTask createDeleteTask() {
        return new DeleteTask();
    }

    /**
     * Create an instance of {@link DeleteTaskResponse }
     * 
     */
    public DeleteTaskResponse createDeleteTaskResponse() {
        return new DeleteTaskResponse();
    }

    /**
     * Create an instance of {@link GetTaskByState }
     * 
     */
    public GetTaskByState createGetTaskByState() {
        return new GetTaskByState();
    }

    /**
     * Create an instance of {@link GetTaskByStateResponse }
     * 
     */
    public GetTaskByStateResponse createGetTaskByStateResponse() {
        return new GetTaskByStateResponse();
    }

    /**
     * Create an instance of {@link SaveTask }
     * 
     */
    public SaveTask createSaveTask() {
        return new SaveTask();
    }

    /**
     * Create an instance of {@link SaveTaskResponse }
     * 
     */
    public SaveTaskResponse createSaveTaskResponse() {
        return new SaveTaskResponse();
    }

    /**
     * Create an instance of {@link UpdateTask }
     * 
     */
    public UpdateTask createUpdateTask() {
        return new UpdateTask();
    }

    /**
     * Create an instance of {@link UpdateTaskResponse }
     * 
     */
    public UpdateTaskResponse createUpdateTaskResponse() {
        return new UpdateTaskResponse();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DatabaseErrorException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DatabaseErrorException }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "DatabaseErrorException")
    public JAXBElement<DatabaseErrorException> createDatabaseErrorException(DatabaseErrorException value) {
        return new JAXBElement<DatabaseErrorException>(_DatabaseErrorException_QNAME, DatabaseErrorException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "deleteTask")
    public JAXBElement<DeleteTask> createDeleteTask(DeleteTask value) {
        return new JAXBElement<DeleteTask>(_DeleteTask_QNAME, DeleteTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "deleteTaskResponse")
    public JAXBElement<DeleteTaskResponse> createDeleteTaskResponse(DeleteTaskResponse value) {
        return new JAXBElement<DeleteTaskResponse>(_DeleteTaskResponse_QNAME, DeleteTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskByState }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskByState }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "getTaskByState")
    public JAXBElement<GetTaskByState> createGetTaskByState(GetTaskByState value) {
        return new JAXBElement<GetTaskByState>(_GetTaskByState_QNAME, GetTaskByState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskByStateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskByStateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "getTaskByStateResponse")
    public JAXBElement<GetTaskByStateResponse> createGetTaskByStateResponse(GetTaskByStateResponse value) {
        return new JAXBElement<GetTaskByStateResponse>(_GetTaskByStateResponse_QNAME, GetTaskByStateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "saveTask")
    public JAXBElement<SaveTask> createSaveTask(SaveTask value) {
        return new JAXBElement<SaveTask>(_SaveTask_QNAME, SaveTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "saveTaskResponse")
    public JAXBElement<SaveTaskResponse> createSaveTaskResponse(SaveTaskResponse value) {
        return new JAXBElement<SaveTaskResponse>(_SaveTaskResponse_QNAME, SaveTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "updateTask")
    public JAXBElement<UpdateTask> createUpdateTask(UpdateTask value) {
        return new JAXBElement<UpdateTask>(_UpdateTask_QNAME, UpdateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "updateTaskResponse")
    public JAXBElement<UpdateTaskResponse> createUpdateTaskResponse(UpdateTaskResponse value) {
        return new JAXBElement<UpdateTaskResponse>(_UpdateTaskResponse_QNAME, UpdateTaskResponse.class, null, value);
    }

}
