
package webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para state complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="state"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deleted" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="panelId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="place" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "state", propOrder = {
    "deleted",
    "id",
    "name",
    "panelId",
    "place"
})
public class State {

    protected boolean deleted;
    protected long id;
    protected String name;
    protected long panelId;
    protected int place;

    /**
     * Obtiene el valor de la propiedad deleted.
     * 
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * Define el valor de la propiedad deleted.
     * 
     */
    public void setDeleted(boolean value) {
        this.deleted = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad panelId.
     * 
     */
    public long getPanelId() {
        return panelId;
    }

    /**
     * Define el valor de la propiedad panelId.
     * 
     */
    public void setPanelId(long value) {
        this.panelId = value;
    }

    /**
     * Obtiene el valor de la propiedad place.
     * 
     */
    public int getPlace() {
        return place;
    }

    /**
     * Define el valor de la propiedad place.
     * 
     */
    public void setPlace(int value) {
        this.place = value;
    }

}
