package utils;

import java.net.MalformedURLException;
import java.net.URL;

import webservices.IUserAuthWS;
import webservices.UserAuthWSImplementationService;

public class AuthenticationCheckerUtils {
	
	public static boolean isUnauthorized(String token) throws MalformedURLException {
		UserAuthWSImplementationService service = new UserAuthWSImplementationService(new URL("http://156.35.98.163:8082/AuthSW/soapws/userauth?wsdl"));
		IUserAuthWS authWS = service.getUserAuthWSImplementationPort();
		
		return authWS.getUserByToken(token) == null;
	}

}
