package consumes.task;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.client.ClientResponse.Status;

import utils.AuthenticationCheckerUtils;
import webservices.DatabaseErrorException_Exception;
import webservices.ITaskWS;
import webservices.Task;
import webservices.TaskWSImplementationService;

@Path("tasks")
public class TaskController {

	private ITaskWS taskWS = null;

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTask(@PathParam("id") long id, @HeaderParam("authorization") String token)
			throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getTaskWS().deleteTask(id)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateTask(Task task, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getTaskWS().updateTask(task)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveTask(Task task, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getTaskWS().saveTask(task)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}

	private ITaskWS getTaskWS() throws MalformedURLException {
		if (taskWS == null) {
			TaskWSImplementationService service = new TaskWSImplementationService(
					new URL("http://156.35.98.163:8081/AppSW/soapws/task?wsdl"));
			taskWS = service.getTaskWSImplementationPort();
		}

		return taskWS;
	}
}