package consumes.state;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.ClientResponse.Status;

import utils.AuthenticationCheckerUtils;
import webservices.DatabaseErrorException_Exception;
import webservices.IStateWS;
import webservices.ITaskWS;
import webservices.State;
import webservices.StateWSImplementationService;
import webservices.Task;
import webservices.TaskWSImplementationService;



@Path("states")
public class StateController {

	private IStateWS stateWS = null;
	private ITaskWS taskWS = null;
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteState(@PathParam("id") long id, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request").build();
		}
		if(getStateWS().deleteState(id)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.ACCEPTED).entity(null).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateState(State state, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request").build();
		}
		if(getStateWS().updateState(state)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveState(State state, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request").build();
		}
		if(getStateWS().saveState(state)) {
			return Response.status(Status.CREATED).build();
		}
		return Response.status(Status.BAD_REQUEST).build();	}
	
	@GET
	@Path("{id}/tasks")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTasksFromState(@PathParam("id") long id, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request").build();
		}
		ObjectMapper mapper = new ObjectMapper();
		List<Task> taskList = getTaskWS().getTaskByState(id);
		String json_response = mapper.writeValueAsString(taskList);
		return Response.status(Status.OK).entity(json_response).build();
	}
	
	private IStateWS getStateWS() throws MalformedURLException {
		if (stateWS == null) {
			StateWSImplementationService service = new StateWSImplementationService(new URL("http://156.35.98.163:8081/AppSW/soapws/state?wsdl"));
			stateWS = service.getStateWSImplementationPort();
		}
		return stateWS;
	}
	
	private ITaskWS getTaskWS() throws MalformedURLException {
		if (taskWS == null) {
			TaskWSImplementationService service = new TaskWSImplementationService(new URL("http://156.35.98.163:8081/AppSW/soapws/task?wsdl"));
			taskWS = service.getTaskWSImplementationPort();
		}
		return taskWS;
	}
}
