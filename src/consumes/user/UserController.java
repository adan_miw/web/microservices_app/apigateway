package consumes.user;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.ClientResponse.Status;

import utils.AuthenticationCheckerUtils;
import webservices.DatabaseErrorException_Exception;
import webservices.IPanelWS;
import webservices.IUserAuthWS;
import webservices.Jwt;
import webservices.Panel;
import webservices.PanelWSImplementationService;
import webservices.User;
import webservices.UserAuthWSImplementationService;

@Path("users")
public class UserController {
	
	private IUserAuthWS userAuth = null;
	private IPanelWS panelWS = null;
	
	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User user) throws IOException {
		User logged = getUserAuth().getUserByEmailAndPassword(user.getEmail(), user.getPassword());
		
		if(logged != null) {
			Jwt token = getUserAuth().generateTokenFromEmail(logged.getEmail());
			Response.ResponseBuilder response = Response.ok(logged);
			response.header("Authorization", token.getToken());
			//return Response.status(Status.CREATED).build();

			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(logged);
			

			return Response.status(Status.OK).header("Authorization", token.getToken())
					.header("Access-Control-Expose-Headers", "Authorization")
					.entity(jsonString).build();

			//return response.build();
		}
		return Response.status(Status.UNAUTHORIZED).build();
	}

	@POST
	@Path("register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(User user) throws IOException {
		if(getUserAuth().saveUser(user)) {		
			return Response.status(Status.CREATED).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	
	
	
	
	@GET
	@Path("{id}/panels")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPanelFromUser(@PathParam("id") long id, @HeaderParam("authorization") String token)
			throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request").build();
		}
		ObjectMapper mapper = new ObjectMapper();
		List<Panel> panelList = getPanelWS().getPanelsFromUser(id);
		String json_response = mapper.writeValueAsString(panelList);
		return Response.status(Status.OK).entity(json_response).build();
	}
	
	
	
	private IPanelWS getPanelWS() throws MalformedURLException {
		if (panelWS == null) {
			PanelWSImplementationService service = new PanelWSImplementationService(new URL("http://156.35.98.163:8081/AppSW/soapws/panel?wsdl"));
			panelWS = service.getPanelWSImplementationPort();
		}

		return panelWS;
	}
	
	
	private IUserAuthWS getUserAuth() throws MalformedURLException {
		if(userAuth == null) {
			UserAuthWSImplementationService service = new UserAuthWSImplementationService(new URL("http://156.35.98.163:8082/AuthSW/soapws/userauth?wsdl"));
			userAuth = service.getUserAuthWSImplementationPort();
		}
		
		return userAuth;
	}
}
