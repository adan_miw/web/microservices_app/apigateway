package consumes.panel;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.ClientResponse.Status;

import utils.AuthenticationCheckerUtils;
import webservices.DatabaseErrorException_Exception;
import webservices.IPanelWS;
import webservices.IStateWS;
import webservices.Panel;
import webservices.PanelWSImplementationService;
import webservices.State;
import webservices.StateWSImplementationService;

@Path("panels")
public class PanelController {

	private IPanelWS panelWS = null;
	private IStateWS stateWS = null;

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePanel(@PathParam("id") long id, @HeaderParam("authorization") String token)
			throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getPanelWS().deletePanel(id)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePanel(Panel panel, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getPanelWS().updatePanel(panel)) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response savePanel(Panel panel, @HeaderParam("authorization") String token) throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		if (getPanelWS().savePanel(panel)) {
			return Response.status(Status.CREATED).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@GET
	@Path("{id}/states")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatesFromPanel(@PathParam("id") long id, @HeaderParam("authorization") String token)
			throws IOException, DatabaseErrorException_Exception {
		if (AuthenticationCheckerUtils.isUnauthorized(token)) {
			return Response.status(Status.FORBIDDEN).entity("The user does not have permission to make the request")
					.build();
		}
		List<State> stateList = getStateWS().getStatesFromPanel(id);
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(stateList);
		return Response.status(Status.OK).entity(jsonString).build();
	}

	private IPanelWS getPanelWS() throws MalformedURLException {
		if (panelWS == null) {
			PanelWSImplementationService service = new PanelWSImplementationService(new URL("http://156.35.98.163:8081/AppSW/soapws/panel?wsdl"));
			panelWS = service.getPanelWSImplementationPort();
		}

		return panelWS;
	}

	private IStateWS getStateWS() throws MalformedURLException {
		if (stateWS == null) {
			StateWSImplementationService service = new StateWSImplementationService(new URL("http://156.35.98.163:8081/AppSW/soapws/state?wsdl"));
			stateWS = service.getStateWSImplementationPort();
		}

		return stateWS;
	}

}